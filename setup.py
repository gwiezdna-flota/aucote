from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='aucote',
    version='0.0.1',
    description='Automated Compliance Tests - Tool for security scans',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/gwiezdna-flota/aucote',
    author='Dominik Rosiek',
    author_email='45dominik@gmail.com',
    classifiers=[  # Optional
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Information Technology',
        'Intended Audience :: Telecommunications Industry',
        'Topic :: Security',

        # Pick your license as you wish
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='aucote security scan nmap masscan',

    packages=find_packages(),
    python_requires='>=3.7, <4',

    install_requires=[
        'pyaml==19.12.0',
        'netaddr==0.7.19',
        'python-dateutil==2.8',
        'netifaces==0.10.9',
        'croniter==0.3.31',
        'pytz==2019.3',
        'inotify==0.2.10',
        'tornado==4.5.3',
        'tornado-crontab==0.4.0',
        'pyOpenSSL==19.1.0',
        'pyasn1==0.4.8',
        'ndg-httpsclient==0.5.1',
        'cpe==1.2.1',
        'ujson==1.35',
        'async-dns==1.0.8',
        'defusedxml==0.6.0',
        'psycopg2==2.8.6',
        'yoyo-migrations==7.0.1',
        ],
    extras_require = {
        'dev': [
            'pylint==2.4.4',
            'nose-cov==1.6',
            'bandit==1.6.2',
            'coverage==5.0.3',
            'nose==1.3.7',
            'Sphinx==2.4.3',
            'sphinx-rtd-theme==0.4.3',
        ]
    },
    scripts=['bin/aucote'],
    data_files=[
      ('config', ['config/aucote_cfg_default.yaml', 'config/aucote_cfg.yaml.example']),
    ],
    include_package_data=True,
    package_data = {
        'aucote': [
            'aucote/fixtures/exploits/exploits.csv',
            '/logins.hydra.txt', 'aucote/static/passwords.hydra.txt',
            'aucote/static/nmap/*.lst',
            ],
    },
    project_urls={
        'Bug Reports': 'https://gitlab.com/gwiezdna-flota/aucote/issues',
        'Source': 'https://gitlab.com/gwiezdna-flota/aucote/',
    }
)
