from unittest import TestCase

from aucote.tools.skipfish.base import SkipfishBase
from aucote.tools.skipfish.parsers import SkipfishOutputParser


class SkipfishBaseTest(TestCase):

    def test_parser(self):
        self.assertIsInstance(SkipfishBase.parser, SkipfishOutputParser)