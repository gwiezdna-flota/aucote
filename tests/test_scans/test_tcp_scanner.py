from unittest import TestCase
from unittest.mock import MagicMock, patch

from aucote.scans.tcp_scanner import TCPScanner


class TCPScannerTest(TestCase):
    def setUp(self):
        self.aucote = MagicMock()
        self.scanner = TCPScanner(aucote=self.aucote, as_service=False, host='localhost', port=1339)

    @patch('aucote.scans.tcp_scanner.PortsScan')
    def test_scanners(self, ports_scan):
        result = self.scanner.scanners
        expected = {
            self.scanner.IPV4: [ports_scan.return_value],
            self.scanner.IPV6: [ports_scan.return_value]
        }

        self.assertEqual(result, expected)
