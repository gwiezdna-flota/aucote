from unittest import TestCase
from unittest.mock import patch, MagicMock, call, mock_open

from tornado.concurrent import Future
from tornado.testing import AsyncTestCase, gen_test

from aucote.aucote_cfg import load, _DEFAULT
from aucote.utils import Config


class AucoteCfgTest(AsyncTestCase):
    YAML = '''alice:
    has:
        a: dog'''

    @patch('aucote.aucote_cfg.log_cfg')
    @patch('aucote.aucote_cfg.cfg', new_callable=Config)
    @patch('os.path.join', MagicMock(return_value='test_default'))
    # Fix abspath
    @patch('os.path.abspath', MagicMock(return_value='test'))
    @gen_test
    async def test_empty_load(self, cfg, log_cfg):
        cfg._cfg = {
            'logging': '',
            'default_config': 'test_default',
            'toucan': {
                'enable': False,
            }
        }
        log_cfg.config.return_value = Future()
        log_cfg.config.return_value.set_result(MagicMock())

        cfg.load = MagicMock()
        cfg.load_toucan = MagicMock()
        await load(file_name='test')
        cfg.load.assert_has_calls([call('test', _DEFAULT), call('test_default', cfg._cfg)], False)

    @patch('os.path.join', MagicMock(return_value='test'))
    @patch('aucote.aucote_cfg.cfg', new_callable=Config)
    @patch('aucote.aucote_cfg.log_cfg', MagicMock())
    @gen_test
    async def test_nonexist_file_load(self, mock_cfg):
        mock_cfg.load = MagicMock(side_effect=FileNotFoundError())
        with self.assertRaises(SystemExit):
            await load('test')

    @patch('aucote.aucote_cfg.cfg', new_callable=Config)
    @patch('aucote.aucote_cfg.log_cfg', MagicMock())
    @patch('os.path.join', MagicMock(return_value='test'))
    @gen_test
    async def test_invalid_file_load(self, mock_cfg):
        mock_cfg.load = MagicMock(side_effect=TypeError())
        with self.assertRaises(SystemExit):
            await load('test')

    @patch('aucote.aucote_cfg.log_cfg')
    @patch('aucote.aucote_cfg.cfg', new_callable=Config)
    @patch('os.path.join', MagicMock(return_value='test'))
    @gen_test
    async def test_invalid_default_file_load(self, cfg, log_cfg):
        cfg._cfg = {
            'logging': '',
            'default_config': 'test_default',
            'toucan': {
                'enable': False,
            }
        }

        log_cfg.config.return_value = Future()
        log_cfg.config.return_value.set_result(MagicMock())

        cfg.load = MagicMock(side_effect=(None, TypeError()))
        with self.assertRaises(SystemExit):
            await load('test')
