"""
Test main aucote file
"""
from unittest.mock import patch, Mock, MagicMock, PropertyMock, mock_open, call
from unittest import skip

import sys
from tornado.concurrent import Future
from tornado.testing import AsyncTestCase, gen_test

from aucote.aucote import main, Aucote
from aucote.structs import TaskManagerType
from aucote.utils import Config
from aucote.utils.exceptions import NmapUnsupported, FeederConnectionException
from aucote.utils.web_server import WebServer


@patch('aucote.aucote_cfg.cfg.load', Mock(return_value=""))
@patch('aucote.utils.log.config', Mock(return_value=""))
@patch('aucote.aucote.os.remove', MagicMock(side_effect=FileNotFoundError()))
class AucoteTest(AsyncTestCase):
    @patch('aucote.aucote.Storage')
    @patch('aucote.aucote.cfg', new_callable=Config)
    @patch('aucote.aucote.AsyncTaskManager', MagicMock())
    def setUp(self, cfg, mock_storage):
        super(AucoteTest, self).setUp()
        self.cfg = cfg

        self.storage = mock_storage
        self.cfg._cfg = {
            'portdetection': {
                'security_scans': ['tools']
            },
            'storage': {
                'db': 'test_storage',
                'fresh_start': True,
                'max_nodes_query': 243
            },
            'service': {
                'scans': {
                    'threads': 30,
                    'parallel_tasks': 30
                },
                'api': {
                    'v1': {
                        'host': None,
                        'port': None
                    },
                    'path': ''
                }
            },
            'kuduworker': {
                'enable': True,
                'queue': {
                    'address': None
                }
            },
            'pid_file': None,
            'fixtures': {
                'exploits': {
                    'filename': '/test'
                }
            },
            'config_filename': 'test',
            'feeder': {
                'api': {
                    'host': 'localhost',
                    'port': '1234',
                    'base': '/api/v1'
                }
            },
            'tcpportscan': {
                'host': 'localhost',
                'port': '1239'
            },
            'tftp': {
                'port': 6969,
                'timeout': 120,
                'host': '127.0.0.1',
                'min_port': 60000,
                'max_port': 61000
            }
        }
        self.aucote = Aucote(exploits=MagicMock(), tools_config=MagicMock())
        self.aucote.ioloop = MagicMock()
        self.aucote._storage_thread = MagicMock()
        self.aucote._tftp_thread = MagicMock()

        for task_manager in self.aucote.async_task_managers.values():
            future = Future()
            future.set_result(MagicMock())
            task_manager.shutdown_condition.wait.return_value = future

        self.aucote._scan_task = MagicMock()
        self.scan_task_run = MagicMock()
        scan_task_future = Future()
        scan_task_future.set_result(self.scan_task_run)
        self.aucote._scan_task.run.return_value = scan_task_future

    @patch('aucote.aucote.cfg_load')
    @patch('builtins.open', mock_open())
    @patch('aucote.aucote.fcntl', MagicMock())
    @patch('aucote.aucote.Aucote')
    @patch('aucote.aucote.cfg', new_callable=Config)
    @gen_test
    async def test_main_scan(self, cfg, mock_aucote, mock_cfg_load):
        mock_cfg_load.return_value = Future()
        mock_cfg_load.return_value.set_result(True)
        cfg._cfg = self.cfg._cfg
        args = PropertyMock()
        args.configure_mock(cmd='scan')
        future = Future()
        future.set_result(MagicMock())
        mock_aucote().run_scan.return_value = future

        with patch('argparse.ArgumentParser.parse_args', return_value=args):
            await main()

        self.assertEqual(mock_aucote.return_value.run_scan.call_count, 1)

    @patch('aucote.aucote.cfg_load')
    @patch('aucote.fixtures.exploits.Exploits.read', MagicMock(side_effect=NmapUnsupported))
    @patch('builtins.open', mock_open())
    @patch('aucote.aucote.fcntl', MagicMock())
    @gen_test
    async def test_main_exploits_exception(self, mock_cfg_load):
        mock_cfg_load.return_value = Future()
        mock_cfg_load.return_value.set_result(True)
        args = PropertyMock()
        args.configure_mock(cmd='scan')

        with patch('argparse.ArgumentParser.parse_args', return_value=args):
            with self.assertRaises(SystemExit):
                await main()

    @patch('aucote.aucote.cfg_load')
    @patch('builtins.open', mock_open())
    @patch('aucote.aucote.fcntl', MagicMock())
    @patch('aucote.aucote.cfg')
    @patch('aucote.aucote.Aucote')
    @patch('aucote.utils.config.Config.get_path', MagicMock())
    @gen_test
    async def test_main_service(self, mock_aucote, mock_cfg, mock_cfg_load):
        mock_cfg_load.return_value = Future()
        mock_cfg_load.return_value.set_result(True)
        args = PropertyMock()
        args.configure_mock(cmd='service')
        future = Future()
        future.set_result(MagicMock())
        mock_aucote.return_value.run_scan.side_effect = (future, SystemExit(), )
        with patch('argparse.ArgumentParser.parse_args', return_value=args):
            with self.assertRaises(SystemExit):
                await main()

        mock_aucote.return_value.run_scan.assert_any_call()
        self.assertEqual(mock_aucote.return_value.run_scan.call_count, 2)
        mock_cfg.reload.assert_called_once_with(mock_cfg.__getitem__.return_value)

    @patch('aucote.scans.executor.Executor.__init__', MagicMock(return_value=None))
    @patch('aucote.aucote.Storage')
    @patch('aucote.aucote.ToolsScanner')
    @patch('aucote.aucote.UDPScanner')
    @patch('aucote.aucote.TCPScanner')
    @patch('aucote.aucote.IOLoop', MagicMock())
    @patch('aucote.aucote.cfg', new_callable=Config)
    @gen_test
    async def test_scan(self, cfg, tcp_scanner, udp_scanner, tools_scanner, mock_storage_task):
        cfg._cfg = self.cfg._cfg
        cfg.register_action = MagicMock()
        self.aucote._thread_pool = MagicMock()
        self.aucote._storage = MagicMock()

        self.mock_web_server()

        self.aucote.async_task_manager = MagicMock()
        self.aucote.async_task_manager.shutdown_condition.wait.return_value = Future()
        self.aucote.async_task_manager.shutdown_condition.wait.return_value.set_result(True)
        self.aucote.async_task_manager.stop = MagicMock(return_value=Future())
        self.aucote.async_task_manager.stop.return_value.set_result(True)

        tcp_scanner.return_value.return_value = Future()
        tcp_scanner.return_value.return_value.set_result(True)

        udp_scanner.return_value.return_value = Future()
        udp_scanner.return_value.return_value.set_result(True)

        await self.aucote.run_scan(as_service=False)

        tcp_scanner.assert_called_once_with(aucote=self.aucote, as_service=False, host='localhost', port=1239)
        udp_scanner.assert_called_once_with(aucote=self.aucote, as_service=False)
        self.assertFalse(tools_scanner.called)

    @patch('aucote.scans.executor.Executor.__init__', MagicMock(return_value=None))
    @patch('aucote.aucote.Storage')
    @patch('aucote.aucote.ToolsScanner')
    @patch('aucote.aucote.UDPScanner')
    @patch('aucote.aucote.TCPScanner')
    @patch('aucote.aucote.IOLoop', MagicMock())
    @patch('aucote.aucote.cfg', new_callable=Config)
    @gen_test
    async def test_service(self, cfg, tcp_scanner, udp_scanner, tools_scanner, mock_storage_task):
        cfg._cfg = self.cfg._cfg
        cfg._consumer = MagicMock()
        self.aucote._storage = MagicMock()

        self.aucote.async_task_managers[TaskManagerType.SCANNER] = MagicMock()
        self.aucote.async_task_managers[TaskManagerType.SCANNER].shutdown_condition.wait.return_value = Future()
        self.aucote.async_task_managers[TaskManagerType.SCANNER].shutdown_condition.wait.return_value.set_result(True)

        self.mock_web_server()

        tcp_scanner.return_value.return_value = Future()
        tcp_scanner.return_value.return_value.set_result(True)

        udp_scanner.return_value.return_value = Future()
        udp_scanner.return_value.return_value.set_result(True)

        tools_scanner.return_value.return_value = Future()
        tools_scanner.return_value.return_value.set_result(True)

        await self.aucote.run_scan(as_service=True)

        tcp_scanner.assert_called_once_with(aucote=self.aucote, as_service=True, host='localhost', port=1239)
        udp_scanner.assert_called_once_with(aucote=self.aucote, as_service=True)
        tools_scanner.assert_called_once_with(aucote=self.aucote, name='tools')
        self.aucote.async_task_managers[TaskManagerType.SCANNER].add_crontab_task.assert_has_calls((
            call(tcp_scanner(), tcp_scanner()._scan_cron),
            call(udp_scanner(), udp_scanner()._scan_cron),
            call(tools_scanner(), tools_scanner()._scan_cron, event='tools')))


    @patch('aucote.aucote.TCPScanner.__init__', MagicMock(side_effect=FeederConnectionException, return_value=None))
    @patch('aucote.aucote.Storage', MagicMock())
    @patch('aucote.scans.scanner.Executor')
    @patch('aucote.aucote.cfg', new_callable=Config)
    @gen_test
    async def test_scan_with_exception(self, cfg, mock_executor):
        self.mock_web_server()

        cfg._cfg = self.cfg._cfg
        cfg._consumer = MagicMock()
        await self.aucote.run_scan()
        self.assertEqual(mock_executor.call_count, 0)

    def test_add_async_task(self):
        self.aucote._thread_pool = MagicMock()
        data = MagicMock()

        self.aucote.add_async_task(data)
        self.aucote.async_task_managers[TaskManagerType.REGULAR].add_task.assert_called_once_with(data)

    @patch('aucote.aucote.cfg_load')
    @patch('builtins.open', mock_open())
    @patch('aucote.aucote.fcntl')
    @gen_test
    async def test_aucote_run_already(self, mock_fcntl, mock_cfg_load):
        mock_cfg_load.return_value = Future()
        mock_cfg_load.return_value.set_result(True)
        mock_fcntl.lockf = MagicMock(side_effect=IOError())
        args = PropertyMock()
        args.configure_mock(cmd='service')
        with patch('argparse.ArgumentParser.parse_args', return_value=args):
            with patch('aucote.aucote.Aucote.run_scan'):
                with self.assertRaises(SystemExit):
                    await main()

    @patch('aucote.aucote.Storage')
    @patch('aucote.aucote.Aucote.load_tools')
    @patch('aucote.aucote.cfg', new_callable=Config)
    @patch('aucote.aucote.AsyncTaskManager', MagicMock())
    def test_init(self, cfg, mock_loader, mock_storage):
        cfg._cfg = self.cfg._cfg
        exploits = MagicMock()
        cfg = MagicMock()

        aucote = Aucote(exploits=exploits, tools_config=cfg)

        mock_storage.assert_called_once_with(conn_string='test_storage', nodes_limit=243)
        self.assertEqual(aucote.storage, mock_storage())
        mock_loader.assert_called_once_with(cfg)

    def test_signal_handling(self):
        self.aucote.kill = MagicMock()
        self.aucote.signal_handler(2, None)
        self.aucote.kill.assert_called_once_with()

    @patch('aucote.aucote.cfg', new_callable=Config)
    @patch('aucote.aucote.Storage', MagicMock())
    def test_load_tools(self, cfg):
        cfg._cfg = self.cfg._cfg
        config = {
            'apps': {
                'app1': {
                    'name': MagicMock,
                    'loader': MagicMock(),
                },
                'app2': {
                    'name': MagicMock,
                    'loader': MagicMock(),
                }
            }
        }

        exploits = MagicMock()
        Aucote(exploits=exploits, tools_config=config)

        config['apps']['app1']['loader'].assert_called_once_with(config['apps']['app1'], exploits)
        config['apps']['app2']['loader'].assert_called_once_with(config['apps']['app2'], exploits)

    @gen_test
    def test_graceful_stop_signal(self):
        self.aucote._scan_task = MagicMock()

        future_1 = Future()
        future_1.set_result(None)
        self.aucote.async_task_managers[TaskManagerType.SCANNER].stop.return_value = future_1

        self.aucote._watch_thread = MagicMock()
        self.aucote.graceful_stop(None, None)

        self.aucote.ioloop.add_callback_from_signal.assert_called_once_with(self.aucote._graceful_stop)

    @gen_test
    async def test_graceful_stop(self):
        future = Future()
        future.set_result(MagicMock())

        self.aucote.async_task_managers = {
            'a': MagicMock(),
            'b': MagicMock()
        }

        for task_manager in self.aucote.async_task_managers.values():
            future_wait = Future()
            future_wait.set_result(MagicMock())
            task_manager.stop.return_value = future_wait

        await self.aucote._graceful_stop()

        for task_manager in self.aucote.async_task_managers.values():
            task_manager.stop.assert_called_once_with()

    @patch('aucote.aucote.os._exit')
    def test_kill(self, mock_kill):
        self.aucote.kill()
        mock_kill.assert_called_once_with(1)

    def test_unfinished_tasks(self):
        self.aucote.async_task_managers[TaskManagerType.REGULAR] = MagicMock(unfinished_tasks=3)
        self.aucote.async_task_managers[TaskManagerType.SCANNER] = MagicMock(unfinished_tasks=4)
        self.aucote.async_task_managers[TaskManagerType.QUICK] = MagicMock(unfinished_tasks=8)
        self.assertEqual(self.aucote.unfinished_tasks, 15)

    def test_python_version(self):
        self.assertGreaterEqual(sys.version_info, (3, 5))

    def get_future(self, result=None):
        future = Future()
        future.set_result(result)
        return future

    def mock_web_server(self):
        self.aucote.web_server = WebServer(MagicMock(), None, None)
        self.aucote.web_server.run = MagicMock(return_value=self.get_future())
        self.aucote.web_server.stop = MagicMock(return_value=self.get_future())

    @patch('aucote.aucote.cfg', new_callable=Config)
    def test_start_scan(self, cfg):
        cfg.toucan = MagicMock()
        key = 'portdetection.test_scan.control.start'
        self.aucote.ioloop = MagicMock()

        self.aucote.start_scan(key, True, 'test_scan')

        cfg.toucan.put.assert_called_once_with(key, False)

    @patch('aucote.aucote.cfg', new_callable=Config)
    def test_start_scan_key_is_false(self, cfg):
        cfg.toucan = MagicMock()
        key = 'portdetection.test_scan.control.start'
        self.aucote.ioloop = MagicMock()

        self.aucote.start_scan(key, False, 'test_scan')

        self.assertFalse(cfg.toucan.called)

    @patch('aucote.aucote.cfg', new_callable=Config)
    def test_start_scan_without_scan_name(self, cfg):
        cfg.toucan = MagicMock()
        key = 'portdetection.test_scan.control.start'
        self.aucote.ioloop = MagicMock()

        self.aucote.start_scan(key, True)
        cfg.toucan.put.assert_called_once_with(key, False)

    @patch('aucote.aucote.cfg', new_callable=Config)
    def test_start_scan_without_scan_name_critical(self, cfg):
        cfg.toucan = MagicMock()
        key = 'portdetecton.test_scan.control.start'
        self.aucote.ioloop = MagicMock()
        with self.assertRaises(KeyError):
            self.aucote.start_scan(key, True)
