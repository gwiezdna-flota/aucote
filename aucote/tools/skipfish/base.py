"""
Provides basic integrations of Skipfish

"""
from aucote.tools.common import Command
from aucote.tools.skipfish.parsers import SkipfishOutputParser


class SkipfishBase(Command):
    """
    Skipfish base class

    """
    COMMON_ARGS = ('-u',)
    NAME = 'skipfish'

    parser = SkipfishOutputParser()
