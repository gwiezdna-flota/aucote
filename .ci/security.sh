#!/bin/bash

PYLINT_FILES=$(find . -name "*.py" -not -path "./tests/*" -not -path "./venv/*")

python3 -m bandit -ll ${PYLINT_FILES} | tee /tmp/bandit.txt
exit ${PIPESTATUS[0]}