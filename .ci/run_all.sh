#!/bin/bash


.ci/unit_tests.sh "${BUILD_URL}/testReport/" "Performs unit tests" "unit tests" || { EXIT_VALUE=1; echo "UNIT TESTS FAILED"; }
.ci/security.sh "${BUILD_URL}/console" "Checks code security" "code security" || { EXIT_VALUE=1; echo "SECURITY TESTS FAILED"; }
.ci/pylint.sh "${BUILD_URL}/testReport/" "Checks code quality" "code quality"
