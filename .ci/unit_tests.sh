#!/bin/bash

FILES=$(find . -maxdepth 1 -type d)
python3 -m nose --verbose --with-xunit --with-coverage --cover-erase --cover-xml --cover-package=. ${FILES} || exit 1
