import argparse
from functools import partial
from http.server import BaseHTTPRequestHandler, HTTPServer

import yaml
import json
import logging as log
import sys

root = log.getLogger()
root.setLevel(log.DEBUG)

handler = log.StreamHandler(sys.stdout)
handler.setLevel(log.DEBUG)
formatter = log.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)


class Feeder(BaseHTTPRequestHandler):

    def __init__(self, config_file: str, *args, **kwargs):
        self.config_file = config_file
        super().__init__(*args, **kwargs)

    def get_hosts(self):
        with open(self.config_file) as file:
            hosts = yaml.safe_load(file)

        return hosts

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(self.get_hosts()).encode('utf8'))
        return


def run():
    log.info('starting server...')

    parser = argparse.ArgumentParser(description='Feeder - the hosts provider')
    parser.add_argument('-c', '--config', help='Hosts config file path', type=str, default='files/hosts_example.yml')
    parser.add_argument('-H', '--host', help='Feeder host name', type=str, default='feeder')
    parser.add_argument('-p', '--port', help='Feeder server port', type=int, default=1234)
    args = parser.parse_args()

    # Server settings
    server_address = (args.host, args.port)
    httpd = HTTPServer(server_address, partial(Feeder, args.config))
    log.info(f'Running Feeder - http://{args.host}:{args.port}')
    httpd.serve_forever()


if __name__ == '__main__':
    run()
