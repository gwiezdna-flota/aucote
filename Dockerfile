FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    software-properties-common \
    libffi-dev \
    python3-setuptools \
    python3-pip \
    python3-dev \
    libyaml-dev \
    libpq-dev \
    curl \
    git \
    nmap \
    masscan

RUN pip3 install pipenv

WORKDIR /opt/aucote
COPY . .

RUN pipenv install --system

